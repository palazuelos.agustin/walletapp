<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;
use App\Transfer;


/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/


$factory->define(Transfer::class, function (Faker $faker) {
    return [
        //
        'description' => $faker->text($maxNbChars = 200 ),
        'amount' => $faker->numberBetween($min = 0, $max = 1000),
        'wallet_id' => $faker->numberBetween(0, 100),
    ];
});
