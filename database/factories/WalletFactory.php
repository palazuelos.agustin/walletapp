<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Wallet;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(Wallet::class, function (Faker $faker) {
    return [
        //
        'money' => $faker->numberBetween($min = 0, $max = 1000),
    ];
});
