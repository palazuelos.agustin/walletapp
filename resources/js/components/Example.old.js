import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import TransferForm from './TransferForm';
import TransferTable from './TransferTable';

export default class Example extends React.Component {
    constructor(props){
        super(props)

        this.state = {
            money: 0.0,
            transfers: [],
            error: null
        }
    }

      static getDerivedStateFromError(error) {
          // Update state so the next render will show the fallback UI.
          return { hasError: true };  }

    async componentDidMount(){
        try {
            let res = await fetch('http://127.0.0.1:8000/api/wallet')
            let data = await res.json()
//             console.log(data)
            this.setState({
                money: data.money,
                transfers: data.transfers
            })
            console.log(this.state);
        } catch (error){

        }
    }


    render(){
        return (
            <div className="container">
                <div className="row justify-content-center">
                    <div className="col-md-12 m-t-md">
                        <p className="title"> $ { this.state.money } </p>
                    </div>
                    <div className="col-md-12" id="transferform">
                        <TransferForm/>
                    </div>
                </div>
                <div className="m-t-md" id="transfertable">
//                    <TransferTable /*transfers={ this.state.transfers }*/ />
                </div>
            </div>

        );
    }
}

// export default Example;

if (document.getElementById('example')) {
    ReactDOM.render(<Example />, document.getElementById('example'));
}
