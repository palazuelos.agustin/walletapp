import React from 'react'
const TransferTable = ({transfers}) => (

    <table className="table">
        <thead>
            <tr>
                <td>Description</td>
                <td>Amount</td>
            </tr>
        </thead>
        <tbody>
            { transfers.map((transfer) => (
//                 console.log(transfer.id+', '+transfer.description+', '+transfer.amount);
                <tr key={ transfer.id }>
                    <td> { transfer.description } </td>
                    <td
                    className={ transfer.amount > 0 ? 'text-succes' : 'text-danger'}>
                        { transfer.amount }
                    </td>
                </tr>
                ))

            }

        </tbody>

    </table>
);
export default TransferTable;
