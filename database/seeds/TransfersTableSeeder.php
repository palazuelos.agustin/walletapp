<?php

use Illuminate\Database\Seeder;

class TransfersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        $arr = array([
//             'description' => 'Salary',
//             'amount' => 3200,
//             'wallet_id' => 1,
//             'created_at' => date("Y-m-d H:i:s"),
//             'updated_at' => date("Y-m-d H:i:s")
//             ],
//             [
            'description' => 'Expenses',
            'amount' => -1000,
            'wallet_id' => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
            ]
        foreach  ($arr as $k => $v {
        DB::table('transfers')->insert($v);
        }
    }
}
