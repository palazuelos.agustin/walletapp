import React from 'react'
const TransferTable = ({ transfers }) => (
    <table className="table">
        <tbody>
            { transfers.map((transfer) => (
                <tr key={ transfer.id }>
                    <td> { transfer.description } </td>
                    <td
                    className={ transfer.amount > 0 ? 'text-succes' : 'text-danger'}>
                        { transfer.amount }
                    </td>
                </tr>
                ))

            }

        </tbody>
    </table>
    )
export default TransferTable
